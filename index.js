import { connect } from 'mongoose'
import bodyParser from 'body-parser'
import express from 'express'
import UserModel from './UserModel'
import cors from 'cors'
const q2m = require('query-to-mongo')

connect(
  //establishing mongo connection
  'mongodb+srv://test-user:17L2WY9bUWXqQmn3bnfPg8lB@maincluster.kwdmr.gcp.mongodb.net/yakkyofy?retryWrites=true&w=majority',
  { useNewUrlParser: true, useUnifiedTopology: true }
)

const app = express()
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.get('/', (req, res) => {
  res.send('Just a test')
})

app.get('/users', async (req, res) => {
  //refactoring the endpoint with query and pagination, using query-to-mongo package
  try {
    const query = q2m(req.query)
    let totalUsers = await UserModel.countDocuments(query.criteria)
    let users = await UserModel.find(query.criteria, query.options.fields)
      .skip(query.options.skip)
      .limit(query.options.limit)
      .sort(query.options.sort)
    res.send({ links: query.links('/users', totalUsers), users })
  } catch (err) {
    res.send(err)
  }

  //getting all of the users in the db
  /* await UserModel.find((err, results) => {
    res.send(results)
  }) */
})

app.post('/users', async (req, res) => {
  //defining a new user and adding properties upon the body of the req
  let user = new UserModel()

  user.email = req.body.email
  user.firstName = req.body.firstName
  user.lastName = req.body.lastName
  user.password = req.body.password

  await user.save((err, newUser) => {
    if (err) res.send(err)
    else res.send(newUser)
  })
})

app.get('/users/:id', async (req, res) => {
  //browse the Db for the matching id to be found
  let user = await UserModel.findById(req.params.id)
  if (user) res.send(user)
  else res.send('user not found')
})

app.put('/users/:id', async (req, res) => {
  //browse the Db for the matching id to be updated
  let user = await UserModel.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true, useFindAndModify: false })
  if (user) {
    res.send(user)
    console.log(user)
  } else res.send('user not found')
})

app.delete('/users/:id', async (req, res) => {
  //browse the Db for the matching id to be deleted
  let user = await UserModel.findOneAndDelete({ _id: req.params.id })
  if (user) res.send('user deleted!')
  else res.send('user not found')
})

app.listen(8080, () => console.log('Example app listening on port 8080!'))
