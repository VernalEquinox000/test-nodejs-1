import { Schema, model } from 'mongoose'

const UserSchema = new Schema({
  email: {
    type: String,
    lowercase: true,
    trim: true,
    required: true,
    unique: true, //added some extra validation parameters below
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address'],
    dropDups: true,
  },
  password: { type: String, required: true, select: false },
  firstName: { type: String, required: [true, 'Name is required'] },
  lastName: { type: String, required: [true, 'Surname is required!'] },
})

export default model('User', UserSchema)
